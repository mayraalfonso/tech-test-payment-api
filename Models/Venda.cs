using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models.Enum;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
       public int Id { get; set; }
        public DateTime Data { get; set; }
        public string CodVenda { get; set; }       
        public int ItemId { get; set; }
        public ICollection<Item> ListaItens { get; set; }
        public int VendedorId { get; set; }        
        public Vendedor Vendedor { get; set; }
        public StatusDaVenda Status { get; set; }
 
    }
}