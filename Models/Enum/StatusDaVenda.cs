using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models.Enum
{
    public enum StatusDaVenda
    {
        AguardandoPagamento = 1,
        PagamentoAprovado = 2,
        EnviadaParaTransportadora = 3,
        Entregue = 4,
        Cancelada = 5

    }
}