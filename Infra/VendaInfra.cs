using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.Context;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;
using tech_test_payment_api.Models.Enum;

namespace tech_test_payment_api.Infra
{
    public class VendaInfra
    {
        private readonly VendaDbContext _context;
        private readonly IMapper _mapper;
        private readonly ItensInfra _itensInfra;

        public VendaInfra(VendaDbContext context, IMapper mapper, ItensInfra itensInfra) {
            _context = context;
            _mapper = mapper;
            _itensInfra = itensInfra;
        }

        public Venda CriarVenda(VendaDTO vendaDTO, int idVendedor)
        {
            var venda = _mapper.Map<Venda>(vendaDTO);
            var vendedor = _context.Vendedores.Where(v => v.Id == idVendedor).FirstOrDefault();
            venda.ListaItens = _itensInfra.AdicionarItens(vendaDTO);
            venda.Vendedor = vendedor;
            venda.Status = Models.Enum.StatusDaVenda.AguardandoPagamento;
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return venda;
        }

        public List<Venda> Listar() {
            return _context.Vendas.ToList();
        }

        public List<Venda> BuscarPorCodVenda(string codVenda) {
            return _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
        }

        public List<Venda> BuscarPorStatusVenda(StatusDaVenda status) {
            return _context.Vendas.Where(v => v.Status == status).ToList();
        }

        public Venda Editar(VendaDTO vendaDTO) {
            var venda = _mapper.Map<Venda>(vendaDTO);
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return venda;
        }

        public Venda Excluir(int id) {
            var venda = _context.Vendas.Where(v => v.Id == id).FirstOrDefault();
            _context.Vendas.Remove(venda);
            _context.SaveChanges();
            return venda;
        }

        public List<Venda> AutorizarPagamento(string codVenda) {
            var listaVendas = _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
            if (listaVendas.Count > 0)
                foreach(Venda venda in listaVendas) {
                    if(venda.Status == Models.Enum.StatusDaVenda.AguardandoPagamento)
                        venda.Status = Models.Enum.StatusDaVenda.PagamentoAprovado;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }

        public List<Venda> EnviarParaTransportadora(string codVenda) {
            var listaVendas = _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
            if (listaVendas.Count > 0)
                foreach(Venda venda in listaVendas) {
                    if(venda.Status == Models.Enum.StatusDaVenda.PagamentoAprovado)
                        venda.Status = Models.Enum.StatusDaVenda.EnviadaParaTransportadora;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }

        public List<Venda> Entregar(string codVenda) {
            var listaVendas = _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
            if (listaVendas.Count > 0)
                foreach(Venda venda in listaVendas) {
                    if(venda.Status == Models.Enum.StatusDaVenda.EnviadaParaTransportadora)
                        venda.Status = Models.Enum.StatusDaVenda.Entregue;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }

        public List<Venda> Cancelar(string codVenda) {
            var listaVendas = _context.Vendas.Where(v => v.CodVenda == codVenda).ToList();
            if (listaVendas.Count > 0)
                foreach(Venda venda in listaVendas) {
                    if(venda.Status == Models.Enum.StatusDaVenda.AguardandoPagamento || venda.Status == Models.Enum.StatusDaVenda.PagamentoAprovado)
                        venda.Status = Models.Enum.StatusDaVenda.Cancelada;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }
    }
}
