using tech_test_payment_api.Context;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;
using AutoMapper;

namespace tech_test_payment_api.Infra
{
    public class VendedorInfra
    {
        private readonly VendaDbContext _context;
        private readonly IMapper _mapper;

        public VendedorInfra(VendaDbContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        }

        public Vendedor CriarVendedor(VendedorDTO vendedorDTO) {
            var vendedor = _mapper.Map<Vendedor>(vendedorDTO);
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
            return vendedor;
        }

        public List<Vendedor> Listar() {
            return _context.Vendedores.ToList();
        }

        public Vendedor BuscarPorCodigoUnico(string codigoUnico) {
            return _context.Vendedores.Where(v => v.CodigoUnico == codigoUnico).FirstOrDefault();
        }

        public Vendedor BuscarPorCpf(string cpf) {
            return _context.Vendedores.Where(v => v.Cpf == cpf).FirstOrDefault();
        }

        public Vendedor Editar(VendedorDTO vendedorDTO) {
            var vendedor = _mapper.Map<Vendedor>(vendedorDTO);
            _context.Vendedores.Update(vendedor);
            _context.SaveChanges();
            return vendedor;
        }

        public Vendedor Excluir(int id) {
            var vendedor = _context.Vendedores.Where(v => v.Id == id).FirstOrDefault();
            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();
            return vendedor;
        }
    }
  }