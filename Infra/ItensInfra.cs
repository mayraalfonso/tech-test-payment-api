using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.Context;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;


namespace tech_test_payment_api.Infra
{
    public class ItensInfra
    {
        private readonly VendaDbContext _context;
        private readonly IMapper _mapper;

        public ItensInfra(VendaDbContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;
        }

        public List<Item> AdicionarItens(List<ItemDTO> itens) {

            var listaItens = _mapper.Map<List<Item>>(itens);
            foreach(var item in listaItens) {
                _context.Itens.Add(item);
            }
            _context.SaveChanges();
            return listaItens;
        }

        public Item CriarItem(ItemDTO itemDTO) {
            var item = _mapper.Map<Item>(itemDTO);
            _context.Itens.Add(item);
            _context.SaveChanges();
            return item;
        }

        public List<Item> Listar() {
            return _context.Itens.ToList();
        }

        public Item BuscarPorCodigoUnico(string codigoUnico) {
            return _context.Itens.Where(i => i.CodigoUnico.Contains(codigoUnico)).FirstOrDefault();
        }

        public Item Editar(ItemDTO itemDTO) {
            var item = _mapper.Map<Item>(itemDTO);
            _context.Itens.Update(item);
            _context.SaveChanges();
            return item;
        }

        public Item Excluir(int id) {
            var item = _context.Itens.Where(i => i.Id == id).FirstOrDefault();
            _context.Itens.Remove(item);
            _context.SaveChanges();
            return item;
        }

        internal ICollection<Item> AdicionarItens(object itens)
        {
            throw new NotImplementedException();
        }
    }
}