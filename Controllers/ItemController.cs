using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Infra;


namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    public class ItemController : ControllerBase
    {
        private ItensInfra _itensInfra;

        public ItemController(ItensInfra itensInfra) {
            _itensInfra = itensInfra;
        }

        [HttpGet("Listar")]
        public IActionResult Listar() {
            var listaItens = _itensInfra.Listar();
            if (listaItens.Count > 0)
                return Ok(listaItens);
            return NotFound("Nenhum item encontrado");
        }

        [HttpGet("BuscarPorCodigoUnico")]
        public IActionResult BuscarPorCodigoUnico(string codigoUnico) {
            var itemPorCodigoUnico = _itensInfra.BuscarPorCodigoUnico(codigoUnico);
            if (itemPorCodigoUnico is not null)
                return Ok(itemPorCodigoUnico);
            return NotFound($"Nenhum item encontrado com o código {codigoUnico}");
        }

        [HttpPost("Cadastrar")]
        public IActionResult Cadastrar(ItemDTO itemDTO) {
            var item = _itensInfra.CriarItem(itemDTO);
            Response.StatusCode = 201;
            return new ObjectResult(new {info = $"Novo item - {item.Nome} - adicionado com sucesso"});
        }

        [HttpPut("Editar")]
        public IActionResult Editar(ItemDTO itemDTO) {
            var item = _itensInfra.Editar(itemDTO);
            return Ok(item);
        }

        [HttpDelete("Excluir/{id}")]
        public IActionResult Excluir(int id) {
            var item = _itensInfra.Excluir(id);
            if (item is not null)
                return Ok($"Item {item.Nome} excluído com sucesso");
            return NotFound($"Nenhum item encontrado com o id {id}");
        }

    }
}