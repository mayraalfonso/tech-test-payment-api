using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Infra;


namespace tech_test_payment_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class VendedoresController : ControllerBase
    {
        private VendedorInfra _vendedorInfra;

        public VendedoresController(VendedorInfra vendedorInfra) {
            _vendedorInfra = vendedorInfra;
        }

        [HttpGet("Listar")]
        public IActionResult Listar() {
            var listaVendedores = _vendedorInfra.Listar();
            if (listaVendedores.Count > 0)
                return Ok(listaVendedores);
            return NotFound("Nenhum(a) vendedor(a) encontrado(a)");
        }

        [HttpGet("BuscarPorCodigoUnico")]
        public IActionResult BuscarPorId(string codigoUnico) {
            var itemPorId = _vendedorInfra.BuscarPorCodigoUnico(codigoUnico);
            if (itemPorId is not null)
                return Ok(itemPorId);
            return NotFound($"Nenhum(a) vendedor(a) encontrado(a) com o código {codigoUnico}");
        }

        [HttpGet("BuscarPorCpf")]
        public IActionResult BuscarPorCpf(string cpf) {
            var vendedorPorCpf = _vendedorInfra.BuscarPorCpf(cpf);
            if (vendedorPorCpf is not null)
                return Ok(vendedorPorCpf);
            return NotFound($"Nenhum(a) vendedor(a) encontrado(a) com o CPF {cpf}");
        }

        [HttpPost("Cadastrar")]
        public IActionResult Cadastrar(VendedorDTO vendedorDTO) {
            var vendedor = _vendedorInfra.CriarVendedor(vendedorDTO);
            Response.StatusCode = 201;
            return new ObjectResult(new {info = $"Novo(a) vendedor(a) - {vendedor.Nome} - adicionado(a) com sucesso"});
        }

        [HttpPut("Editar")]
        public IActionResult Editar(VendedorDTO vendedorDTO) {
            var vendedor = _vendedorInfra.Editar(vendedorDTO);
            return Ok(vendedor);
        }

        [HttpDelete("Excluir/{id}")]
        public IActionResult Excluir(int id) {
            var vendedor = _vendedorInfra.Excluir(id);
            if (vendedor is not null)
                return Ok($"Vendedor(a) {vendedor.Nome} excluído(a) com sucesso");
            return NotFound($"Nenhum(a) vendedor(a) encontrado(a) com o id {id}");
        }
    }
}