using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using tech_test_payment_api.Context;
using tech_test_payment_api.Infra;
using tech_test_payment_api.Mapping;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;

namespace tech_test_payment_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
        
            services.AddScoped<ItensInfra>();
            services.AddScoped<VendedorInfra>();
            services.AddScoped<VendaInfra>();
            services.AddDbContext<VendaDbContext>(options => 
                 options.UseSqlServer(Configuration.GetConnectionString("ConexaoPadrao"))
                .UseLoggerFactory(LoggerFactory.Create(ModelBuilder => ModelBuilder.AddConsole()))
                );
            services.AddAutoMapper(typeof(DTOMapa));
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "tech_test_payment_api", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "tech_test_payment_api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}