using System.Text.Json.Serialization;
using tech_test_payment_api.Infra;

namespace tech_test_payment_api.DTO
{
    public class ItemDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public string CodigoUnico { get; set; }
        public string Descricao { get; set; }
        public string Nome { get; set; }
    }

}