using System.Text.Json.Serialization;
using tech_test_payment_api.Models;
using tech_test_payment_api.Models.Enum;

namespace tech_test_payment_api.DTO
{
    public class VendaDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string CodVenda { get; set; }       
        public int ItemId { get; set; }
        public ICollection<Item> ListaItens { get; set; }
        public int VendedorId { get; set; }        
        public Vendedor Vendedor { get; set; }
        public StatusDaVenda Status { get; set; }
    }

}