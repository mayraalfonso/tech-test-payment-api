using AutoMapper;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Mapping
{
    public class DTOMapa : Profile
    {
        public DTOMapa() {
            CreateMap<Item, ItemDTO>().ReverseMap();
            CreateMap<Vendedor, VendedorDTO>().ReverseMap();
            CreateMap<Venda, VendaDTO>().ReverseMap();
        }
    }
}
